#!/usr/bin/python
from math import floor
from algos.unionfind import UnionFindOptimized

class DynamicConnectivity(object):
  """
    The classic problem of dynamic connectivity can be solved using union find
  """
  def __init__(self, n):
    self.n = n
    self.t = n * n
    self.opened = set()
    self.uf_1 = UnionFindOptimized(self.t+2)
    self.uf_2 = UnionFindOptimized(self.t+1)
    self.state = [[0 for i in range(self.n)] for j in range(self.n)]

  def open(self, r, c):
    idx = self.row_col_to_idx(r, c);
    north = self.row_col_to_idx(r-1, c);
    east = self.row_col_to_idx(r, c+1);
    south = self.row_col_to_idx(r+1, c);
    west = self.row_col_to_idx(r, c-1);

    if r > 1 and north in self.opened:
      self.uf_1.union(idx, north);
      self.uf_2.union(idx, north);

    if c < self.n and east in self.opened:
      self.uf_1.union(idx, east);
      self.uf_2.union(idx, east);

    if r < self.n and south in self.opened:
      self.uf_1.union(idx, south);
      self.uf_2.union(idx, south);

    if c > 1 and west in self.opened:
      self.uf_1.union(idx, west);
      self.uf_2.union(idx, west);

    if r == 1:
      self.uf_1.union(idx, self.t);
      self.uf_2.union(idx, self.t);

    if r == self.n:
      self.uf_1.union(idx, self.t+1);

    self.opened.add(idx)

  def is_open(self, r, c):
    return self.row_to_col_idx(r, c) in self.opened

  def is_full(self, r, c):
    idx = self.row_col_to_idx(r, c)
    return self.uf_2.connected(idx, self.t)

  def open_site_count(self):
    return len(self.opened)

  def percolates(self):
    return self.uf_1.connected(self.t, self.t+1)

  def row_col_to_idx(self, r, c):
    return (self.n * (r-1) + c) - 1

  def idx_to_row_col(self, i):
    return 1 + floor(i/self.n), 1 + i % self.n

  def get_state(self):
    for i in self.opened:
      r, c = self.idx_to_row_col(i)
      f = self.is_full(r, c)
      self.state[r-1][c-1] = 2 if f else 1
    return self.state

if __name__ == '__main__':
  from random import shuffle
  import sys, time, curses

  n = 1024
  dc = DynamicConnectivity(n)

  # Call this to run the simulation
  dc = DynamicConnectivity(n)
  idx = [i for i in range(0, dc.t)]
  shuffle(idx)
  index = 0

  while not dc.percolates():
    # open a random site in the network and send the new data to 
    dc.open(*dc.idx_to_row_col(idx[index]))
    index += 1 

  print(f'Complete after {index} iterations.')
