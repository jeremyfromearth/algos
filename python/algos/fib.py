def fib_slow(n):
  if n <= 1: 
    return n
  return fib_slow(n-1) + fib_slow(n-2)

def fib_fast(n):
  f = [0 for i in range(n+1)]
  f[0] = 0
  f[1] = 1
  for i in range(2, n+1):
    f[i] = f[i-1] + f[i-2]
  return f[n]
