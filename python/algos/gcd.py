'''
Return the greatest common divisor
'''
def gcd_slow(a, b):
  n = 0
  for i in range(2, a+b):
    if a % i == 0 and b % i == 0:
      n = i
  return n

''' 
Euclidean Algorithm
'''
def gcd_fast(a, b):
  if b == 0:
    return a

  '''
  #recursive version
  ap = a % b
  return gcd_fast(b, ap)
  '''

  # non recursive
  while(b != 0):
    pb = b
    ap = a % b
    a = pb
    b = ap
  return a
