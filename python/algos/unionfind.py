class UnionFindNaive(object):
  """ 
    Naive implementation of UnionFind algorithm. 
  """
  def __init__(self, n):
    # Time Complexity: O(N)
    self.idx = list(range(n))

  def union(self, a, b):
    # Time Complexity: O(N)
    x = self.idx[a]
    y = self.idx[b]
    for i, r in enumerate(self.idx):
      if r == x:
        self.idx[i] = y

  def connected(self, a, b):
    # Time Complexity: O(1)
    return self.idx[a] == self.idx[b]

class UnionFind(object):
  """
    Improved Union Find by including root() method.
    union() only involves writing one index, however worst case is still O(N)
  """
  def __init__(self, n):
    # Time Complexity: O(N)
    self.idx = list(range(n))

  def union(self, a, b):
    # Time Complexity (worst case): O(N)
    x = self.root(a)
    y = self.root(b)
    if x == y: return
    self.idx[y] = x

  def connected(self, a, b):
    # Time Complexity (worst case): O(N)
    return self.root(a) == self.root(b)

  def root(self, i):
    while i != self.idx[i]:
      i = self.idx[i]
    return i

class UnionFindOptimized(UnionFind):
  """ Optimized Union Find uses weighting and path compression. """
  
  def __init__(self, n):
    self.size = [1] * n
    self.idx = list(range(n))

  def union(self, a, b):
    x = self.root(a)
    y = self.root(b)

    # Time Complexity: O(1)
    if x == y: return
    if self.size[x] < self.size[y]: # Weighting
      self.idx[x] = y
      self.size[y] += self.size[x]
    else:
      self.idx[y] = x
      self.size[x] += self.size[y]

  def root(self, i):
    # Time Complexity (worst case): O(lg N)
    while i != self.idx[i]:
      self.idx[i] = self.idx[self.idx[i]] # Path compression
      i = self.idx[i]
    return i

  def __repr__(self):
    return '[UnionFindOptimized]\n\tIndex: {}\n\tSize:  {}'.format(self.idx, self.size)

if __name__ == '__main__':
  
  pass
